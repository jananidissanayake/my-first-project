#include <stdio.h>
int main()
{
  int a, b, t;

  printf("Enter two integers\n");
  scanf("%d%d", &a, &b);

  printf("Before Swapping\nFirst integer = %d\nSecond integer = %d\n", a, b);

  t = a;
  a = b;
  b = t;

  printf("After Swapping\nFirst integer = %d\nSecond integer = %d\n", a, b);

  return 0;
}
