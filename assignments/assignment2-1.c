#include <stdio.h> 
  
int main() 
{ 
    int no; 
  
    printf("Enter the number no: "); 
    scanf("%d", &no); 
  
    if (no > 0) 
        printf("%d is positive.", no); 
    else if (no < 0) 
        printf("%d is negative.", no); 
    else if (no == 0) 
        printf("%d is zero.", no); 
  
    return 0; 
} 
